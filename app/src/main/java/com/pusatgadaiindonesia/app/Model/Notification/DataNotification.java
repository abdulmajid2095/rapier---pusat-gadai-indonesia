package com.pusatgadaiindonesia.app.Model.Notification;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataNotification {
    @SerializedName("data")
    private List<DataNotificationDetail> data;
    

    public DataNotification(List<DataNotificationDetail> data) {
        this.data = data;
    }

    public List<DataNotificationDetail> getdata() {
        return data;
    }
}
