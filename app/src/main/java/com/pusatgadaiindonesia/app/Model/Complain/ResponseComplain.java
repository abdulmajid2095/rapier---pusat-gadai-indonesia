package com.pusatgadaiindonesia.app.Model.Complain;

import com.google.gson.annotations.SerializedName;
import com.pusatgadaiindonesia.app.Model.Login.DataLogin;

public class ResponseComplain {
    @SerializedName("code")
    private String code;

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    public ResponseComplain(String code, String status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }

    public String getcode() {
        return code;
    }

    public String getstatus() {
        return status;
    }

    public String getmessage() {
        return message;
    }
}
