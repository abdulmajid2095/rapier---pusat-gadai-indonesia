package com.pusatgadaiindonesia.app.Model.Login;

import com.google.gson.annotations.SerializedName;

public class ResponseLogin {
    @SerializedName("code")
    private String code;

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private DataLogin data;

    public ResponseLogin(String code, String status, String message, DataLogin data) {
        this.code = code;
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getcode() {
        return code;
    }

    public String getstatus() {
        return status;
    }

    public String getmessage() {
        return message;
    }

    public DataLogin getdata() {
        return data;
    }

}
