package com.pusatgadaiindonesia.app.Model.Login;

import com.google.gson.annotations.SerializedName;

public class SendLogin {
    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    public SendLogin(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
