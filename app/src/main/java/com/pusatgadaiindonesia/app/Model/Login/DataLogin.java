package com.pusatgadaiindonesia.app.Model.Login;

import com.google.gson.annotations.SerializedName;

public class DataLogin {
    @SerializedName("token")
    private String token;

    @SerializedName("userId")
    private String userId;

    @SerializedName("userName")
    private String userName;

    @SerializedName("userEmail")
    private String userEmail;

    public DataLogin(String token, String userId, String userName, String userEmail) {
        this.token = token;
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
    }

    public String gettoken() {
        return token;
    }

    public String getuserId() {
        return userId;
    }

    public String getuserName() {
        return userName;
    }

    public String getuserEmail() {
        return userEmail;
    }

}
