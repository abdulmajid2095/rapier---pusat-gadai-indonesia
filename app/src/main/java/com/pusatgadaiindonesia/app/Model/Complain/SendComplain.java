package com.pusatgadaiindonesia.app.Model.Complain;

import com.google.gson.annotations.SerializedName;

public class SendComplain {
    @SerializedName("description")
    private String description;

    @SerializedName("type")
    private String type;

    public SendComplain(String description, String type) {
        this.description = description;
        this.type = type;
    }
}
