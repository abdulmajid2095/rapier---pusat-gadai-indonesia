package com.pusatgadaiindonesia.app.Model.Banner;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseBanner {
    @SerializedName("code")
    private String code;

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    List<DataBannerDetail> data;
    //private DataBanner data;

    public ResponseBanner(String code, String status, String message, List<DataBannerDetail> data) {
        this.code = code;
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getcode() {
        return code;
    }

    public String getstatus() {
        return status;
    }

    public String getmessage() {
        return message;
    }

    public List<DataBannerDetail> getdata() {
        return data;
    }

}
