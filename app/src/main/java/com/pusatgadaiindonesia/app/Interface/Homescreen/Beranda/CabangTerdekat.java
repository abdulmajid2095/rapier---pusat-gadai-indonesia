package com.pusatgadaiindonesia.app.Interface.Homescreen.Beranda;

import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pusatgadaiindonesia.app.Database.Data_Profile;
import com.pusatgadaiindonesia.app.Model.Location.DataLocation;
import com.pusatgadaiindonesia.app.Model.Location.ResponseLocation;
import com.pusatgadaiindonesia.app.R;
import com.pusatgadaiindonesia.app.Services.Retrofit.APIService;
import com.pusatgadaiindonesia.app.Services.Retrofit.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

public class CabangTerdekat extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.cari)
    CardView cari;

    @BindView(R.id.editCari)
    EditText editCari;

    GoogleMap mMap;
    Dialog loading;

    List<DataLocation> listAllDataLocation = new ArrayList<>();
    LatLng nearestLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_cabang_terdekat);
        ButterKnife.bind(this);

        SupportMapFragment mapFragment =(SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView2);
        mapFragment.getMapAsync(this);

        back.setOnClickListener(this);
        cari.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.back:
                onBackPressed();
                break;

            case R.id.cari:
                //Toast.makeText(getApplicationContext(),"Fitur pencarian Belum Tersedia", Toast.LENGTH_SHORT).show();
                loadLocation(editCari.getText().toString());
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        /*LatLng sydney = new LatLng(-6.2402391, 106.8688949);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Pusat Gadai Indonesia"));
        //.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_utama_bpkb))
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(sydney)
                .zoom(17)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),2000,null);*/
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        loadLocation("");
    }

    public void dialogLoading() {
        // @M - showing the loading dialog
        loading = new Dialog(this);
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.dialog_loading);
        loading.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loading.setCancelable(false);
    }

    public void dialogWarning(String message)
    {
        // @M - showing the warning dialog in old style dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_warning_1button);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView textMessage = dialog.findViewById(R.id.teks);
        textMessage.setText(""+message);

        Button oke = dialog.findViewById(R.id.ok);
        oke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void loadLocation(String keyword)
    {
        dialogLoading();
        mMap.clear();
        Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
        String tokenBearer = "Bearer" + " " +data_profile.token;

        String baseUrl = ""+ApiClient.BASE_URL;

        APIService service = ApiClient.getClient().create(APIService.class);
        Call<ResponseLocation> userCall = service.getAllLocation(tokenBearer, baseUrl+"location/branch?search="+keyword);
        userCall.enqueue(new Callback<ResponseLocation>() {
            @Override
            public void onResponse(Call<ResponseLocation> call, retrofit2.Response<ResponseLocation> response) {
                if(response.isSuccessful())
                {
                    String status = ""+response.body().getstatus();
                    Toast.makeText(getApplicationContext(),""+response.body().getmessage(),Toast.LENGTH_SHORT).show();
                    if(status.equals("success"))
                    {
                        listAllDataLocation = new ArrayList<>();
                        List<DataLocation> arrayku = response.body().getdata().getdata();
                        if(arrayku.size() > 0)
                        {
                            Boolean nearestLoaded = false;
                            for (int  i=0; i<arrayku.size();  i++)
                            {
                                DataLocation dataLocation = new DataLocation(
                                        ""+arrayku.get(i).getid(),
                                        ""+arrayku.get(i).getname(),
                                        ""+arrayku.get(i).getLatitude(),
                                        ""+arrayku.get(i).getLongitude(),
                                        ""+arrayku.get(i).getAddress(),
                                        ""+arrayku.get(i).getProvince(),
                                        ""+arrayku.get(i).getCity()
                                );
                                listAllDataLocation.add(dataLocation);

                                try {
                                    Double xLat = 0.0;
                                    Double xLng = 0.0;
                                    xLat = Double.parseDouble(""+arrayku.get(i).getLatitude());
                                    xLng = Double.parseDouble(""+arrayku.get(i).getLongitude());
                                    LatLng latLng = new LatLng(xLat, xLng);
                                    Marker mark = mMap.addMarker(new MarkerOptions().position(latLng).title(""+arrayku.get(i).getname()));
                                    if(nearestLoaded == false)
                                    {
                                        mark.showInfoWindow();
                                        nearestLatLng = latLng;
                                        nearestLoaded = true;
                                    }

                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                }
                            }

                            CameraPosition cameraPosition = new CameraPosition.Builder()
                                    .target(nearestLatLng)
                                    .zoom(17)
                                    .build();
                            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),2000,null);

                        }
                        else
                        {
                            dialogWarning("Data Lokasi Kosong");
                        }
                    }
                    else
                    {
                        dialogWarning(""+response.body().getmessage());
                    }

                }
                else
                {
                    // @M - If response from server is not success, get the message and show it in dialog
                    String message = "Gagal. Silakan coba lagi";
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        message = ""+jObjError.getString("message");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    dialogWarning(""+message);
                }
                loading.dismiss();
            }
            @Override
            public void onFailure(Call<ResponseLocation> call, Throwable t) {
                Log.w("Merk","Error : "+t.getMessage());
                loading.dismiss();
                dialogWarning(""+t.getMessage());
            }
        });
    }
}
