package com.pusatgadaiindonesia.app.Interface.Homescreen;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import com.google.android.material.tabs.TabLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pusatgadaiindonesia.app.R;
import com.vistrav.ask.Ask;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/*HOmscreen bug tampilan
        Edit Profil error (permission ?)'
        Semua gadai gaisa
        gagal load profile
        masih ada bug pada data kendaraan, ketika kendaraan awal memilih motor maka opsi yang
        disediakan pada jenis merk merupakan merk motor namun ketika kendaraan berganti mobil
        maka opsi yang disediakan pada jenis merk tetap merk motor bukan merk mobil dan berlaku sebaliknya
        ketika kolom opsi ditarik kebawah, logo refresh stuck tidak bisa hilang
        gagal load profile, edit prpofile, logout*/

public class Homescreen extends AppCompatActivity implements View.OnClickListener {

    // @M - Tab
    @BindView(R.id.beranda)
    LinearLayout beranda;

    @BindView(R.id.kontrak)
    LinearLayout kontrak;

    @BindView(R.id.profil)
    LinearLayout profil;

    @BindView(R.id.bantuan)
    LinearLayout bantuan;

    @BindView(R.id.imgBeranda)
    ImageView imgBeranda;

    @BindView(R.id.imgKontrak)
    ImageView imgKontrak;

    @BindView(R.id.imgProfil)
    ImageView imgProfil;

    @BindView(R.id.imgBantuan)
    ImageView imgBantuan;

    @BindView(R.id.txtBeranda)
    TextView txtBeranda;

    @BindView(R.id.txtKontrak)
    TextView txtKontrak;

    @BindView(R.id.txtProfil)
    TextView txtProfil;

    @BindView(R.id.txtBantuan)
    TextView txtBantuan;

    // @M - End of Tab

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;


    private int[] tabSelected = {
            R.drawable.menu1_selected,
            R.drawable.menu2_selected,
            R.drawable.menu3_selected,
            R.drawable.menu4_selected
    };

    private int[] tabUnselected = {
            R.drawable.menu1_unselected,
            R.drawable.menu2_unselected,
            R.drawable.menu3_unselected,
            R.drawable.menu4_unselected
    };

    int press = 0;

    View v1,v2,v3,v4;
    ImageView img1,img2,img3,img4;
    TextView text1,text2,text3,text4;
    TabLayout.Tab tab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_homescreen);

        ButterKnife.bind(this);

        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight(0);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //tab.setIcon(tabIcons2[tab.getPosition()]);
                /*Fragment frg = getSupportFragmentManager().findFragmentByTag("android:switcher:"+R.id.viewpager+":"+viewPager.getCurrentItem());
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.detach(frg);
                ft.attach(frg);
                ft.commit();*/
                if(tab.getPosition()==0)
                {
                    imgBeranda.setImageResource(tabSelected[0]);
                    txtBeranda.setTextColor(getResources().getColor(R.color.blue_selected));
                    imgKontrak.setImageResource(tabUnselected[1]);
                    txtKontrak.setTextColor(getResources().getColor(R.color.grey_unselected));
                    imgProfil.setImageResource(tabUnselected[2]);
                    txtProfil.setTextColor(getResources().getColor(R.color.grey_unselected));
                    imgBantuan.setImageResource(tabUnselected[3]);
                    txtBantuan.setTextColor(getResources().getColor(R.color.grey_unselected));
                }
                else if(tab.getPosition()==1)
                {
                    imgBeranda.setImageResource(tabUnselected[0]);
                    txtBeranda.setTextColor(getResources().getColor(R.color.grey_unselected));
                    imgKontrak.setImageResource(tabSelected[1]);
                    txtKontrak.setTextColor(getResources().getColor(R.color.blue_selected));
                    imgProfil.setImageResource(tabUnselected[2]);
                    txtProfil.setTextColor(getResources().getColor(R.color.grey_unselected));
                    imgBantuan.setImageResource(tabUnselected[3]);
                    txtBantuan.setTextColor(getResources().getColor(R.color.grey_unselected));

                }
                else if(tab.getPosition()==2)
                {
                    imgBeranda.setImageResource(tabUnselected[0]);
                    txtBeranda.setTextColor(getResources().getColor(R.color.grey_unselected));
                    imgKontrak.setImageResource(tabUnselected[1]);
                    txtKontrak.setTextColor(getResources().getColor(R.color.grey_unselected));
                    imgProfil.setImageResource(tabSelected[2]);
                    txtProfil.setTextColor(getResources().getColor(R.color.blue_selected));
                    imgBantuan.setImageResource(tabUnselected[3]);
                    txtBantuan.setTextColor(getResources().getColor(R.color.grey_unselected));

                }
                else if(tab.getPosition()==3)
                {
                    imgBeranda.setImageResource(tabUnselected[0]);
                    txtBeranda.setTextColor(getResources().getColor(R.color.grey_unselected));
                    imgKontrak.setImageResource(tabUnselected[1]);
                    txtKontrak.setTextColor(getResources().getColor(R.color.grey_unselected));
                    imgProfil.setImageResource(tabUnselected[2]);
                    txtProfil.setTextColor(getResources().getColor(R.color.grey_unselected));
                    imgBantuan.setImageResource(tabSelected[3]);
                    txtBantuan.setTextColor(getResources().getColor(R.color.blue_selected));


                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                if(tab.getPosition()==0)
                {
                    imgBeranda.setImageResource(tabUnselected[0]);
                    txtBeranda.setTextColor(getResources().getColor(R.color.grey_unselected));
                }
                else if(tab.getPosition()==1)
                {
                    imgKontrak.setImageResource(tabUnselected[1]);
                    txtKontrak.setTextColor(getResources().getColor(R.color.grey_unselected));
                }
                else if(tab.getPosition()==2)
                {
                    imgProfil.setImageResource(tabUnselected[2]);
                    txtProfil.setTextColor(getResources().getColor(R.color.grey_unselected));
                }
                else if(tab.getPosition()==3)
                {
                    imgBantuan.setImageResource(tabUnselected[3]);
                    txtBantuan.setTextColor(getResources().getColor(R.color.grey_unselected));
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Fragment frg = getSupportFragmentManager().findFragmentByTag("android:switcher:"+R.id.viewpager+":"+viewPager.getCurrentItem());
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.detach(frg);
                ft.attach(frg);
                ft.commit();
            }
        });

        beranda.setOnClickListener(this);
        kontrak.setOnClickListener(this);
        profil.setOnClickListener(this);
        bantuan.setOnClickListener(this);
        //permission();
        checkPermission();
        setupTabIcons();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentBeranda(), "");
        adapter.addFragment(new FragmentKontrak(), "");
        adapter.addFragment(new FragmentProfile(), "");
        adapter.addFragment(new FragmentBantuan(), "");
        viewPager.setAdapter(adapter);
    }


    private void setupTabIcons() {

        v1 = getLayoutInflater().inflate(R.layout.custome_tab, null);
        img1 = (ImageView) v1.findViewById(R.id.icontab);
        img1.setImageResource(tabSelected[0]);
        text1 = (TextView) v1.findViewById(R.id.texttab);
        text1.setText(""+getResources().getString(R.string.beranda));
        text1.setTextColor(getResources().getColor(R.color.blue_selected));
        tabLayout.getTabAt(0).setCustomView(v1);

        v2 = getLayoutInflater().inflate(R.layout.custome_tab, null);
        img2 = (ImageView) v2.findViewById(R.id.icontab);
        img2.setImageResource(tabUnselected[1]);
        text2 = (TextView) v2.findViewById(R.id.texttab);
        text2.setText(""+getResources().getString(R.string.kontrak));
        text2.setTextColor(getResources().getColor(R.color.grey_unselected));
        tabLayout.getTabAt(1).setCustomView(v2);

        v3 = getLayoutInflater().inflate(R.layout.custome_tab, null);
        img3 = (ImageView) v3.findViewById(R.id.icontab);
        img3.setImageResource(tabUnselected[2]);
        text3 = (TextView) v3.findViewById(R.id.texttab);
        text3.setText(""+getResources().getString(R.string.profil));
        text3.setTextColor(getResources().getColor(R.color.grey_unselected));
        tabLayout.getTabAt(2).setCustomView(v3);

        v4 = getLayoutInflater().inflate(R.layout.custome_tab, null);
        img4 = (ImageView) v4.findViewById(R.id.icontab);
        img4.setImageResource(tabUnselected[3]);
        text4 = (TextView) v4.findViewById(R.id.texttab);
        text4.setText(""+getResources().getString(R.string.bantuan));
        text4.setTextColor(getResources().getColor(R.color.grey_unselected));
        tabLayout.getTabAt(3).setCustomView(v4);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.beranda:
                tab = tabLayout.getTabAt(0);
                tab.select();
                break;

            case R.id.kontrak:
                tab = tabLayout.getTabAt(1);
                tab.select();
                break;

            case R.id.profil:
                tab = tabLayout.getTabAt(2);
                tab.select();
                break;

            case R.id.bantuan:
                tab = tabLayout.getTabAt(3);
                tab.select();
                break;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            //return mFragmentTitleList.get(position);
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        if(press==0)
        {
            Fragment frg = getSupportFragmentManager().findFragmentByTag("android:switcher:"+R.id.viewpager+":"+viewPager.getCurrentItem());
            final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.detach(frg);
            ft.attach(frg);
            ft.commit();
            Toast.makeText(getApplicationContext(), "Press again to exit", Toast.LENGTH_SHORT).show();
            press = 1;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    press = 0;
                }
            },3000);
        }
        else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            press = 0;
        }

    }

    public void permission()
    {
        Ask.on(this)
                .id(1) // in case you are invoking multiple time Ask from same activity or fragment
                .forPermissions(Manifest.permission.ACCESS_COARSE_LOCATION
                        , Manifest.permission.ACCESS_FINE_LOCATION
                        , Manifest.permission.ACCESS_NETWORK_STATE
                        , Manifest.permission.READ_EXTERNAL_STORAGE
                        , Manifest.permission.WRITE_EXTERNAL_STORAGE
                        , Manifest.permission.READ_PHONE_STATE)
                /*.withRationales("Location permission need for map to work properly",
                        "In order to save file you will need to grant storage permission") *///optional
                .go();
    }



    public void checkPermission()
    {
        int permission = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
        int permission2 = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        int permission3 = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_NETWORK_STATE);
        int permission4 = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
        int permission5 = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE);
        int permission6 = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA);
        int permission7 = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permission8 = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        Log.e("Permission", permission+","+permission2+","+permission3+","+permission4+","+permission5+","+permission6+","+permission7+","+permission8);
        if (permission != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(Homescreen.this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    341);
        }
        else if (permission2 != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(Homescreen.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    342);
        }
        else if (permission3 != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(Homescreen.this,
                    new String[]{Manifest.permission.ACCESS_NETWORK_STATE},
                    343);
        }
        else if (permission4 != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(Homescreen.this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    344);
        }
        else if (permission5 != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(Homescreen.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    345);
        }
        else if (permission7 != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(Homescreen.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    347);
        }
        else if (permission8 != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(Homescreen.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    348);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            checkPermission();
        }
        else
        {
            /*location 341, 342
            phone 343, 344, 345
            storage 347, 348*/
            if(requestCode == 341 || requestCode == 342)
            {
                dialogPermission();
            }
            else if(requestCode == 343 || requestCode == 344 || requestCode == 345)
            {
                dialogPermissionPhone();
            }
            else
            {
                dialogPermissionStorage();
            }

        }
    }


    public void dialogPermission()
    {
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_permission);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);

        Button ok = (Button) dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission();
            }
        });

        dialog.show();
    }

    public void dialogPermissionPhone()
    {
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_permission_call);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);

        Button ok = (Button) dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission();
            }
        });

        dialog.show();
    }

    public void dialogPermissionStorage()
    {
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_permission_storage);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);

        Button ok = (Button) dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission();
            }
        });

        dialog.show();
    }
}
