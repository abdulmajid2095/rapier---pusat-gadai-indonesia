package com.pusatgadaiindonesia.app.Database;

import com.orm.SugarRecord;

public class Data_Profile extends SugarRecord {
    public String token;
    public boolean login;

    public String userId, userName, userEmail;


    public Data_Profile() {
    }

    public Data_Profile(String token, Boolean login, String userId, String userName, String userEmail) {

        this.token = token;
        this.login = login;
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
    }
}