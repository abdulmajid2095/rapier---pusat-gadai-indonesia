package com.pusatgadaiindonesia.app;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.pusatgadaiindonesia.app.Database.Data_Profile;
import com.pusatgadaiindonesia.app.Interface.Auth.Login;
import com.pusatgadaiindonesia.app.Interface.Homescreen.Homescreen;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        Data_Profile data = Data_Profile.findById(Data_Profile.class,1L);
        if((int)data.count(Data_Profile.class, "", null) == 0) {
            Data_Profile datax = new Data_Profile("", false, "", "", "");
            datax.save();
        }
        // @M - redirect to homesceen after 2 seconds loading
        Handler handler =  new Handler();
        Runnable myRunnable = new Runnable() {
            public void run() {
                // do something
                finish();
                Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
                if(data_profile.login == true)
                {
                    Intent intent = new Intent(MainActivity.this, Homescreen.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(MainActivity.this, Login.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }


            }
        };
        handler.postDelayed(myRunnable,2000);
    }
}
